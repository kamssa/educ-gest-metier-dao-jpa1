package ci.kossovo.educ.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import ci.kossovo.educ.entites.Evaluation;




public interface EvaluationRepository extends JpaRepository<Evaluation, Long> {

	
}

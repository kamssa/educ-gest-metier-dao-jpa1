package ci.kossovo.educ.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import ci.kossovo.educ.entites.Promotion;




public interface PromotionRepository extends JpaRepository<Promotion, Long> {

}

package ci.kossovo.educ.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import ci.kossovo.educ.entites.TypeEvaluation;



public interface TypeEvaRepository extends JpaRepository<TypeEvaluation, Long> {

}

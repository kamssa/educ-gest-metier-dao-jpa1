package ci.kossovo.educ.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import ci.kossovo.educ.entites.Cours;

public interface CoursRepository extends JpaRepository<Cours, Long> {

}

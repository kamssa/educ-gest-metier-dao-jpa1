package ci.kossovo.educ.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import ci.kossovo.educ.entites.Matiere;
import java.lang.String;
import java.util.List;





public interface MatiereRepository extends JpaRepository<Matiere, Long> {
List<Matiere> findByLibelle(String libelle);
@Query("select mat from Matiere mat where mat.libelle like:x")
List<Matiere> chercherMatierparMc(@Param("x") String mc);
}

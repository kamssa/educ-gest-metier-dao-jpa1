package ci.kossovo.educ.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ci.kossovo.educ.entites.Administrateur;
import ci.kossovo.educ.entites.Enseignant;
import ci.kossovo.educ.entites.Etudiant;
import ci.kossovo.educ.entites.Personne;

public interface PersonneRepository extends JpaRepository<Personne, Long> {

	@Query("select etu from Etudiant etu ")
	List<Personne> findAllEtudiants();

	@Query("select en from Enseignant en ")
	List<Personne> findAllEnseignants();

	@Query("select ad from Administrateur ad ")
	List<Personne> findAllAdministrateurs();

	List<Personne> findByNomCompletContainingIgnoreCase(String nomcomplet);

	@Query("select e from Etudiant e where e.matricule=?1")
	Personne getMatriculeIgnoreCase(String matricule);

	@Query("select en from Enseignant en where UPPER(en.statut)=UPPER(?1) ")
	List<Personne> getStautIgnoreCase(String statut);

	@Query("select ad from Administrateur ad where ad.fonction=?1")
	List<Personne> getFonctionIgnoreCase(String fonction);
	
	
	@Query("select et from Etudiant et where et.nomComplet like %?1%")
	List<Personne> findAllEtudiantsParMc(String mc);

	List<Personne> findByType(String type);

	Personne findByCni(String cni);

}

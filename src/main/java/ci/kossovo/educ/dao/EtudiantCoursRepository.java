package ci.kossovo.educ.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import ci.kossovo.educ.entites.Etudiant;


public interface EtudiantCoursRepository extends JpaRepository<Etudiant, Long> {

}

package ci.kossovo.educ.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import ci.kossovo.educ.entites.EtudiantPromo;


public interface EtudiantPromoRepository extends JpaRepository<EtudiantPromo, Long> {

}

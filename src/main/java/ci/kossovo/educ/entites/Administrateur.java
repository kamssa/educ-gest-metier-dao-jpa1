package ci.kossovo.educ.entites;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "T_ADMINISTEUR")
@DiscriminatorValue("AD")
public class Administrateur extends Personne {

	private static final long serialVersionUID = 1L;
	private String fonction;

	public Administrateur() {
		super();

	}

	

	public Administrateur(String titre, String nom, String prenom, String cni, String fonction) {
		super(titre, nom, prenom, cni);
		this.fonction = fonction;
	}




	public String getFonction() {
		return fonction;
	}

	public void setFonction(String fonction) {
		this.fonction = fonction;
	}

	@Override
	public String toString() {
		return String.format("Administrateur[%s]", super.toString());
	}

}

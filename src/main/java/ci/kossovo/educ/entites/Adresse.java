package ci.kossovo.educ.entites;

import java.io.Serializable;
import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Embeddable;

@Embeddable
public class Adresse implements Serializable{

	
private static final long serialVersionUID = 1L;
private String codePostal;
private String quartier;
private String ville;
private String email;

private String mobile;
private String bureau;
private String fixe;



public Adresse() {
	super();
	// TODO Auto-generated constructor stub
}
public Adresse(String ville, String email) {
	super();
	this.ville = ville;
	this.email = email;
	
}
public String getCodepostal() {
	return codePostal;
}
public void setCodepostal(String codePostal) {
	this.codePostal = codePostal;
}
public String getVille() {
	return ville;
}
public void setVille(String ville) {
	this.ville = ville;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public String getQuartier() {
	return quartier;
}
public void setQuartier(String quartier) {
	this.quartier = quartier;
}
public String getCodePostal() {
	return codePostal;
}
public void setCodePostal(String codePostal) {
	this.codePostal = codePostal;
}
public String getMobile() {
	return mobile;
}
public void setMobile(String mobile) {
	this.mobile = mobile;
}
public String getBureau() {
	return bureau;
}
public void setBureau(String bureau) {
	this.bureau = bureau;
}
public String getFixe() {
	return fixe;
}
public void setFixe(String fixe) {
	this.fixe = fixe;
}



}

package ci.kossovo.educ.entites;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="T_INVITE")
@DiscriminatorValue("IN")
public class Invite extends Personne {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String raison;
	private String profil;
	private String societe;
	private String statut;
	public Invite() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	public Invite(String titre, String nom, String prenom, String cni) {
		super(titre, nom, prenom, cni);
	}


	public String getRaison() {
		return raison;
	}
	public void setRaison(String raison) {
		this.raison = raison;
	}
	public String getProfil() {
		return profil;
	}
	public void setProfil(String profil) {
		this.profil = profil;
	}
	public String getSociete() {
		return societe;
	}
	public void setSociete(String societe) {
		this.societe = societe;
	}
	public String getStatut() {
		return statut;
	}
	public void setStatut(String statut) {
		this.statut = statut;
	}
	
	
	
	
}

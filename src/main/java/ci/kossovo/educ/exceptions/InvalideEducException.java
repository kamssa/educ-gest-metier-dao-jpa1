package ci.kossovo.educ.exceptions;

public class InvalideEducException extends Exception {

	private static final long serialVersionUID = 1L;

	public InvalideEducException(String message) {
		super(message);
		
	}

}

package ci.kossovo.educ;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EducGestMetierDaoJpa1Application {

	public static void main(String[] args) {
		SpringApplication.run(EducGestMetierDaoJpa1Application.class, args);
	}
}

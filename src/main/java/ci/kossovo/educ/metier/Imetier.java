package ci.kossovo.educ.metier;

import java.util.List;

import ci.kossovo.educ.exceptions.InvalideEducException;



public interface Imetier <T,U>{
	
	public T creer(T entity) throws InvalideEducException;
	
	public T modifier(T entity) throws InvalideEducException;
	
	public List<T> findAll();
	
	public T findById(U id);
	
	public boolean supprimer(U id);
	
	public boolean supprimer(List<T> entites);
	
	public boolean existe(U id);
	

}

package ci.kossovo.educ.metier;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ci.kossovo.educ.dao.MatiereRepository;
import ci.kossovo.educ.entites.Matiere;
import ci.kossovo.educ.exceptions.InvalideEducException;

@Service
public class matierMetierImpl implements ImatiereMetier {

	@Autowired
	MatiereRepository matierRepository;
	@Override
	public Matiere creer(Matiere entity) throws InvalideEducException {
		if ((entity.getLibelle()== null)|| (entity.getLibelle()=="")) 
		{
			throw new InvalideEducException("Le libelle ne doit pas etre null ou vide");
		}
		List<Matiere> mats = matierRepository.findByLibelle(entity.getLibelle());
		if(!mats.isEmpty()){
			throw new InvalideEducException("ce libelle existe deja");
		}
		return matierRepository.save(entity) ;
	}
	@Override
	public List<Matiere> findByLibelle(String libelle) {
		
		return matierRepository.findByLibelle(libelle);
	}
// modifier une matiere
	@Override
	public Matiere modifier(Matiere entity) throws InvalideEducException {
		if ((entity.getLibelle()== null)|| (entity.getLibelle()=="")) {
			throw new InvalideEducException("Le libelle ne doit pas etre null ou vide");
		}
		
		return matierRepository.save(entity) ;
	}
// obtenir toutes les matieres
	@Override
	public List<Matiere> findAll() {
		
		return matierRepository.findAll();
	}
// obtenir une matiere a partir de son identifiant
	@Override
	public Matiere findById(Long id) {
		
		return matierRepository.findOne(id);
	}
// supprimer une matuiere a partir de son identifiant
	@Override
	public boolean supprimer(Long id) {
		
		 matierRepository.delete(id);
		 return true;
	}

	@Override
	public boolean supprimer(List<Matiere> entites) {
		matierRepository.delete(entites);
		return true;
	}

	@Override
	public boolean existe(Long id) {
		// TODO Auto-generated method stub
		matierRepository.exists(id);
		return true;
	}
	@Override
	public List<Matiere> chercherMatiereParMc(String mc) {
		
		return matierRepository.chercherMatierparMc(mc);
	}
	
	

}

package ci.kossovo.educ.metier;

import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ci.kossovo.educ.dao.PersonneRepository;
import ci.kossovo.educ.entites.Personne;
import ci.kossovo.educ.exceptions.InvalideEducException;

@Service
public class PersonneMetierImpl implements IPersonneMetier {
	@Autowired
	private PersonneRepository personneRepository;

	@Override
	public Personne creer(Personne entity) throws InvalideEducException 
	{
		if ((entity.getNom() == null) || (entity.getPrenom() == null) || (entity.getCni() == null)
				|| (entity.getNom() == "") || (entity.getPrenom() == "") || (entity.getCni() == ""))
		{
			throw new InvalideEducException("Le nom, le prenom ou cni ne peut etre null");
		}

		Personne pers = null;
		try {
			pers = personneRepository.findByCni(entity.getCni());
		} catch (Exception e) {
			throw new InvalideEducException("probleme de connexion");
		}
		if (pers != null)
			throw new InvalideEducException("ce cni est existe deja");

		return personneRepository.save(entity);

	}

	@Override
	public Personne modifier(Personne entity) throws InvalideEducException {

		Personne p = personneRepository.findByCni(entity.getCni());

		if (p != null && p.getId() != entity.getId()) {

			throw new InvalideEducException("cette carte d'identite est deja utilise");

		}

		return personneRepository.save(entity);

	}

	@Override
	public List<Personne> findAll() {
		return personneRepository.findAll();

	}

	@Override
	public Personne findById(Long id) {

		return personneRepository.findOne(id);
	}

	@Override
	public boolean supprimer(Long id) {
		personneRepository.delete(id);
		return true;
	}

	@Override
	public boolean supprimer(List<Personne> entites) {
		personneRepository.delete(entites);
		return true;
	}

	@Override
	public boolean existe(Long id) {
		return personneRepository.exists(id);
	}

	@Override
	public Personne chercherParMartricule(String matricule) {
		return personneRepository.getMatriculeIgnoreCase(matricule);
	}

	@Override
	public List<Personne> chercherEtudiantParMc(String mc) {
		
		return personneRepository.findAllEtudiantsParMc(mc);
	}

	@Override
	public List<Personne> chercherEnseignantParMc(String mc) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Personne> listeEtudiants() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Personne> listeEnserignant() {
		return null;
	}

	@Override
	public List<Personne> personneALL(String type) {
		List<Personne> pers = personneRepository.findAll();

		List<Personne> typePersonnes = pers.stream().filter(p -> p.getType().equals(type)).collect(Collectors.toList());

		return typePersonnes;

	}

	@Override
	public List<Personne> chercherParStatut(String statut) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Personne> chercherParFonction(String fonction) {
		// TODO Auto-generated method stub
		return null;
	}

}

package ci.kossovo.educ.metier;

import java.util.List;

import ci.kossovo.educ.entites.Matiere;

public interface ImatiereMetier extends Imetier<Matiere,Long> {
List<Matiere> findByLibelle(String libelle);
List<Matiere> chercherMatiereParMc(String mc);
}

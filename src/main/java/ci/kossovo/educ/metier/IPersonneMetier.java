package ci.kossovo.educ.metier;

import java.util.List;

import ci.kossovo.educ.entites.Personne;

public interface IPersonneMetier extends Imetier<Personne, Long> {
	
  Personne chercherParMartricule(String matricule);
 
 List<Personne>chercherParStatut(String statut);
 
 List<Personne>chercherParFonction(String fonction);
 
 List<Personne>chercherEtudiantParMc(String mc);
 
 List<Personne>chercherEnseignantParMc(String mc);
 
 List<Personne> listeEtudiants();
 
 List<Personne>listeEnserignant();
 
  List<Personne> personneALL(String type);
	
 
}

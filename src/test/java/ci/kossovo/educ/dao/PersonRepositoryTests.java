package ci.kossovo.educ.dao;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import ci.kossovo.educ.entites.Administrateur;
import ci.kossovo.educ.entites.Enseignant;
import ci.kossovo.educ.entites.Etudiant;
import ci.kossovo.educ.entites.Invite;
import ci.kossovo.educ.entites.Personne;

@RunWith(SpringRunner.class)
@DataJpaTest
public class PersonRepositoryTests {
	@Autowired
	private TestEntityManager entityManager;
	@Autowired
	private PersonneRepository personneRepository;

	@Test
	public void findAllEtudiants() {
		this.entityManager.persist(new Invite("Mr", "Diarra", "Drissa", "A01"));
		this.entityManager.persist(new Enseignant("Mme", "Kone", "Kandia", "A02", "Permanent"));
		this.entityManager.persist(new Administrateur("Mr", "Jack", "Baeur", "A03", "admin"));
		this.entityManager.persist(new Etudiant("Mr", "Bill", "Gate", "A04", "E100"));
		List<Personne> etudiants = personneRepository.findAllEtudiants();
		assertThat(etudiants.size()).isEqualTo(1);

	}

	@Test
	public void findbyType() {

		this.entityManager.persist(new Invite("Mr", "Diarra", "Drissa", "A01"));
		this.entityManager.persist(new Enseignant("Mme", "Kone", "Kandia", "A02", "Permanent"));
		this.entityManager.persist(new Invite("Mr", "Jack", "Baeur", "A03"));
		this.entityManager.persist(new Etudiant("Mr", "Bill", "Gate", "A04", "E100"));

		List<Personne> etudiants = personneRepository.findByType("ET");
		assertThat(etudiants.size()).isEqualTo(1);

		List<Personne> ens = personneRepository.findByType("EN");
		assertThat(ens.size()).isEqualTo(1);

		List<Personne> inv = personneRepository.findByType("IN");
		assertThat(inv.size()).isEqualTo(2);

	}

	@Test
	public void getMatriculeIgnoreCase() {

		this.entityManager.persist(new Etudiant("Mr", "Bill", "Gate", "A04", "E100"));
		this.entityManager.persist(new Etudiant("Mr", "Traore", "Abdoulaye", "A05", "E101"));

		Etudiant pers = (Etudiant) personneRepository.getMatriculeIgnoreCase("E100");
		assertThat(pers.getMatricule()).isEqualTo("E100");

	}
	
	
	@Test
	public void findByCni() {

		this.entityManager.persist(new Etudiant("Mr", "Bill", "Gate", "A04", "E100"));
		this.entityManager.persist(new Etudiant("Mr", "Traore", "Abdoulaye", "A05", "E101"));

		Etudiant pers = (Etudiant) personneRepository.findByCni("A04");
		assertThat(pers.getCni()).isEqualTo("A04");

	}

	@Test
	public void getStatusIgnoreCase() {

		this.entityManager.persist(new Enseignant("Mme", "Kone", "Kandia", "A02", "Permanent"));
		this.entityManager.persist(new Enseignant("Mr", "Bakoyoko", "Hamed", "A05", "journalier"));

		List<Personne> ens = personneRepository.getStautIgnoreCase("Permanent");
		Enseignant e = (Enseignant) ens.get(0);
		assertThat(e.getStatut()).isEqualTo("Permanent");

	}

	@Test
	public void supprimerTous() {

		this.entityManager.persist(new Invite("Mr", "Diarra", "Drissa", "A01"));
		this.entityManager.persist(new Enseignant("Mme", "Kone", "Kandia", "A02", "Permanent"));
		this.entityManager.persist(new Invite("Mr", "Jack", "Baeur", "A03"));
		this.entityManager.persist(new Etudiant("Mr", "Bill", "Gate", "A04", "E100"));

		List<Personne> pers = personneRepository.findAll();

		assertThat(pers.size()).isEqualTo(4);

		personneRepository.delete(pers);
		List<Personne> pers1 = personneRepository.findAll();
		assertThat(pers1.size()).isEqualTo(0);

	}

	
	@Test
	public void findAllEtudiantParMc() {

		this.entityManager.persist(new Etudiant("Mr", "Diarra", "Bill", "A01","E200"));
		this.entityManager.persist(new Enseignant("Mme", "Kone", "Kandia", "A02", "Permanent"));
		this.entityManager.persist(new Invite("Mr", "Jack", "Baeur", "A03"));
		this.entityManager.persist(new Etudiant("Mr", "Bill", "Gate", "A04", "E100"));

		List<Personne> pers = personneRepository.findAllEtudiantsParMc("Bi");

		assertThat(pers.size()).isEqualTo(2);


	}
	
	@Test
	public void trouverPersonneParMc() {

		this.entityManager.persist(new Invite("Mr", "Diarra", "Drissa", "A01"));
		this.entityManager.persist(new Enseignant("Mme", "Kone", "Kandia", "A02", "Permanent"));
		this.entityManager.persist(new Administrateur("Mr", "Jack", "Kone", "A03", "admin"));
		this.entityManager.persist(new Etudiant("Mr", "Bill", "Gate", "A04", "E100"));

		List<Personne> pers = personneRepository.findByNomCompletContainingIgnoreCase("kone");
		assertThat(pers.size()).isEqualTo(2);

	}

	
	 
	
	
	  
	  @Test public void testCrudJpaRepository(){ 

			personneRepository.save(new Invite("Mr", "Diarra", "Drissa", "A01"));
			personneRepository.save(new Enseignant("Mme", "Kone", "Kandia", "A02", "Permanent"));
			personneRepository.save(new Administrateur("Mr", "Jack", "Kone", "A03", "admin"));
			personneRepository.save(new Etudiant("Mr", "Bill", "Gate", "A04", "E100"));
		  
			List<Personne> pers = personneRepository.findAll();
		    assertThat(pers.size()).isEqualTo(4);
		
		Long id = pers.get(0).getId();
		Personne pe = personneRepository.findOne(id);
		
		assertNotNull(pe);
		
		pe.setNom("kone");
		pe = personneRepository.save(pe);
		
		assertThat(pe.getNom()).isEqualTo("kone");
		personneRepository.delete(id);
		assertThat(personneRepository.findAll().size()).isEqualTo(3);
		
		List<Personne> pers1 = personneRepository.findAll();
		Long id1 = pers1.get(0).getId();
		
		assertThat(id).isNotEqualTo(id1);
	}
	 
}

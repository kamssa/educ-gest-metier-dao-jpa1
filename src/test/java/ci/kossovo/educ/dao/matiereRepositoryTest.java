package ci.kossovo.educ.dao;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import ci.kossovo.educ.entites.Matiere;

@RunWith(SpringRunner.class)
@DataJpaTest
public class matiereRepositoryTest {
	@Autowired
	private TestEntityManager entityManager;
	@Autowired
	private MatiereRepository matiereRepository;

	@Test
	public void creeMatiere() {
		this.entityManager.persist(new Matiere("analyse", "pour la creation de modele"));
		this.entityManager.persist(new Matiere("java", "pour mise en place de des application"));
		this.entityManager.persist(new Matiere("reseau", "pour la communication de l'information"));
		this.entityManager.persist(new Matiere("math", "pour les math info"));
		this.entityManager.persist(new Matiere("architecture", "pour la comprehansion du fonctionement des ordi"));

		List<Matiere> matieres = matiereRepository.findAll();
		assertThat(matieres.size()).isEqualTo(5);
		
		Long id = matieres.get(0).getId();
		Matiere mat = matiereRepository.findOne(id);
		assertNotNull(mat);
		mat.setLibelle("analyse UML");
		mat= matiereRepository.save(mat);
		
		assertThat(mat.getLibelle()).isEqualTo("analyse UML");
		matiereRepository.delete(id);
		
		// aller recuperer les enregistrement dans la base de donnee et comparer la taille a 4
		
		assertThat(matiereRepository.findAll().size()).isEqualTo(4);
		
		assertNull(matiereRepository.findOne(id));
		
		assertNotNull(matiereRepository.save(new Matiere("Angular", "programmation asynchrone")));
	    assertThat(matiereRepository.findAll().size()).isEqualTo(5);
	}

}

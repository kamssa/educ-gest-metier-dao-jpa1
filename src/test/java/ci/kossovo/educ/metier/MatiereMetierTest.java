package ci.kossovo.educ.metier;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import ci.kossovo.educ.dao.MatiereRepository;
import ci.kossovo.educ.entites.Etudiant;
import ci.kossovo.educ.entites.Invite;
import ci.kossovo.educ.entites.Matiere;
import ci.kossovo.educ.entites.Personne;
import ci.kossovo.educ.exceptions.InvalideEducException;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MatiereMetierTest {
	@Autowired
	private ImatiereMetier matiereMetier;

	@MockBean
	private MatiereRepository repositoryMock;

	@Test
	public void creerUneMatiere() {
		// given
		Matiere m1 = new Matiere("java", "langage evolue");
		Matiere m2 = new Matiere("C","langage evolue");
		m2.setId(2L);

		given(repositoryMock.save(m1)).willReturn(m2);
		// when
		Matiere p3 = null;
		try {
			p3 = matiereMetier.creer(m1);
		} catch (InvalideEducException e) {
			e.printStackTrace();
		}
		verify(repositoryMock).save(m1);
		assertThat(p3).isEqualTo(m2);

	}
	@ Test
	public void creerUneMatiereSansLibelle(){
		// given
				Matiere m1 = new Matiere();
				given(repositoryMock.save(m1)).willThrow(new RuntimeException("Le libelle ne peut etre null"));
	        // when
				Matiere mat = null;
				try {
					mat= matiereMetier.creer(m1);
				} catch (Exception e) {
					// TODO: handle exception
				}
				verify(repositoryMock, never()).save(m1);
				assertThat(mat).isEqualTo(null);
	}

	@Test
	public void modifierUneMatiere() {
		// given
		
		Matiere m2 = new Matiere("C","langage evolue");
		m2.setId(2L);
        List<Matiere> mats = new ArrayList<>();
        
	     given(repositoryMock.findByLibelle("java")).willReturn(mats);
		m2.setLibelle("java");
		given(repositoryMock.save(m2)).willReturn(m2);
		// when
		Matiere m3 = null;
		try {
			m3 = matiereMetier.modifier(m2);
		} catch (InvalideEducException e) {
			e.printStackTrace();
		}
		verify(repositoryMock).save(m2);
		assertThat(m3.getLibelle()).isEqualTo("java");

	}
	/*@Test
	public void modifierUneMatiereParLibelleExistant() {
		// given
		
		Matiere m1 = new Matiere("java","langage evolue");
		Matiere m2 = new Matiere("java9","langage");
		m1.setId(1L);
		m2.setId(2L);
        List<Matiere> mats = new ArrayList<>();
        mats.add(m2);
	     given(repositoryMock.findByLibelle("java9")).willReturn(mats);
		m1.setLibelle("java9");
		given(repositoryMock.save(m2)).willReturn(m2);
		// when
		Matiere m3 = null;
		try {
			m3 = matiereMetier.modifier(m2);
		} catch (InvalideEducException e) {
			e.printStackTrace();
		}
		verify(repositoryMock,never()).save(m2);
		assertThat(m3).isEqualTo(null);

	}*/
}

package ci.kossovo.educ.metier;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import ci.kossovo.educ.dao.PersonneRepository;
import ci.kossovo.educ.entites.Enseignant;
import ci.kossovo.educ.entites.Etudiant;
import ci.kossovo.educ.entites.Invite;
import ci.kossovo.educ.entites.Personne;
import ci.kossovo.educ.exceptions.InvalideEducException;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PersonneMetierTest {

	@Autowired
	private IPersonneMetier personneMetier;

	@MockBean
	private PersonneRepository repositoryMock;

	@Test
	public void creerUnePesonne() {
		// given
		Personne p1 = new Invite("Mr", "Diarra", "Drissa", "CN001");
		Personne p2 = new Etudiant("Mr", "Diarra", "Drissa", "EN001", "A014");
		p2.setId(2L);

		given(repositoryMock.save(p1)).willReturn(p2);
		// when
		Personne p3 = null;
		try {
			p3 = personneMetier.creer(p1);
		} catch (InvalideEducException e) {
			e.printStackTrace();
		}
		verify(repositoryMock).save(p1);
		assertThat(p3).isEqualTo(p2);

	}

	@Test
	public void creerUnePersonneSansNomPrenomEtCni() {
		Personne pe = new Invite();
		pe.setTitre("Mr");
		given(repositoryMock.save(pe))
				.willThrow(new RuntimeException("le nom ou le prenom ou le cni ne peut etre null"));

		Personne p = null;
		try {
			p = personneMetier.creer(pe);
		} catch (InvalideEducException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		verify(repositoryMock, never()).save(pe);
		assertThat(p).isEqualTo(null);
	}

	@Test
	public void creerUnePersonneQuiExiste() {
		Personne p1 = new Invite("Mr", "Traore", "Abdoulaye", "A003");
		p1.setId(3L);

		given(repositoryMock.findByCni(p1.getCni())).willReturn(p1);

		Personne pers = null;
		try {
			Personne p2 = new Invite("Mr", "Diarra", "Drissa", "A003");
			pers = personneMetier.creer(p2);
		} catch (InvalideEducException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		verify(repositoryMock).findByCni(p1.getCni());
		verify(repositoryMock, never()).save(p1);
		assertThat(pers).isEqualTo(null);
	}

	@Test
	public void modifierUnePersonne() {
		// given

		Personne p1 = new Invite("Mr", "Traore", "Abdoulaye", "A003");
		p1.setId(4L);

		given(repositoryMock.findByCni(p1.getCni())).willReturn(p1);

		p1.setNom("Diarra");
		given(repositoryMock.save(p1)).willReturn(p1);
		Personne p2 = null;

		try {
			p2 = personneMetier.modifier(p1);

		} catch (InvalideEducException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		verify(repositoryMock).findByCni(p1.getCni());

		verify(repositoryMock).save(p1);
		assertThat(p2.getNom()).isEqualTo("Diarra");

	}

	@Test
	public void modifierPersonneAvecCniExistant() {
		// given

		Personne entityExistant = new Invite("Mr", "Traore", "Abdoulaye", "A003");
		entityExistant.setId(4L);

		Personne modifaApprte = entityExistant;
		modifaApprte.setCni("A004");

		Personne personneProprietaireCni = new Invite("Mr", "Soulama", "Oumar", "A004");
		;
		personneProprietaireCni.setId(6L);

		given(repositoryMock.findByCni("A004")).willReturn(personneProprietaireCni);
		given(repositoryMock.save(modifaApprte)).willThrow(new RuntimeException("cette personne existe"));

		Personne p3 = null;

		try {
			p3 = personneMetier.modifier(modifaApprte);

		} catch (InvalideEducException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		verify(repositoryMock).findByCni("A004");
		verify(repositoryMock, never()).save(modifaApprte);
		assertThat(p3).isEqualTo(null);
	}

	@Test
	public void trouverDesPerspnneParType() {

		Personne p1 = new Invite("Mr", "Diarra", "Drissa", "CN001");
		p1.setId(1L);
		p1.setType("IN");

		Personne p2 = new Etudiant("Mr", "Diarra", "Drissa", "EN001", "A014");
		p2.setId(2L);
		p2.setType("ET");

		Personne p3 = new Enseignant("Mr", "Diarra", "Drissa", "EN001", "permanent");

		p3.setId(3L);
		p3.setType("EN");
		

		List<Personne> personnes = Arrays.asList(p1, p2, p3);
		
		given(repositoryMock.findAll()).willReturn(personnes);
		
		List<Personne> etudiants = personneMetier.personneALL("ET");
		Etudiant etudiant = (Etudiant) etudiants.get(0);
		 
		List<Personne> ens = personneMetier.personneALL("EN");
		Enseignant en = (Enseignant) ens.get(0);
		
		List<Personne> invs = personneMetier.personneALL("IN");
		Invite in = (Invite) invs.get(0);
		
		assertNotNull(etudiant);
		assertThat(etudiants.size()).isEqualTo(1);
		assertThat(etudiant.getMatricule()).isEqualTo("A014");
		
		assertThat(ens.size()).isEqualTo(1);
		assertThat(en.getStatut()).isEqualTo("permanent");
		
		
		assertThat(invs.size()).isEqualTo(1);
		assertThat(in.getNom()).isEqualTo("Diarra");
		
		
		
		

	}

}
